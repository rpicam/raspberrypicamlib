/**
 * @file PiCam_App.h
 * @author Prakash Dhungana (dhunganaprakas@gmail.com)
 * @brief <b> Header file for main application </b>
 * @version 
 * @date 2022-04-06 
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/** Doxygen compliant formatting for comments */

/*===========================[  Compile Flags  ]=========================================*/

#ifndef PICAM_APP_H
#define  PICAM_APP_H

/*===========================[  Inclusions  ]=============================================*/

#include <stddef.h>
#include <stdio.h>

/*============================[  Defines  ]==============================================*/
/** \addtogroup picam_defines   Macros	  
 *  @{
 */

/** Usecase Identification to save captured buffer only */
#define  UC_CAPTURE                 (0x00u)

/** Usecase Identification to save Sobel edges of the captured buffer */
#define  UC_EDGE_SOBEL              (0x01u)

/** Usecase Identification to save Canny edges of the captured buffer  */
#define  UC_EDGE_CANNY              (0x02u)

/** Usecase Identification to save filtered image using Gaussain filter of the captured buffer  */
#define  UC_FILTER_GAUSSIAN         (0x03u)

/** Usecase Identification to save filtered image using mean filter of size 3 of the captured buffer */
#define  UC_FILTER_MEAN3            (0x04u)

/** Usecase Identification to save filtered image using mean filter of size 5 of the captured buffer */
#define  UC_FILTER_MEAN5            (0x05u)

/** Usecase Identification to save filtered image using median filter of size 3 of the captured buffer */
#define  UC_FILTER_MEDIAN3          (0x06u)

/** Usecase Identification to save filtered image using median filter of size 5 of the captured buffer */
#define  UC_FILTER_MEDIAN5          (0x07u)

/** Usecase Identification to save rotated image of the captured buffer */
#define  UC_EDIT_ROTATE             (0x08u)

/** Usecase Identification to save upscaled image of the captured buffer */
#define  UC_EDIT_UPSCALE            (0x09u)

/** Usecase Identification to save downscaled image of the captured buffer */
#define  UC_EDIT_DOWNSCALE          (0x0Au)

/** Usecase Identification to save resized image of the captured buffer */
#define  UC_EDIT_RESIZE             (0x0Bu)

/** Usecase Identification to save horizontally flipped image of the captured buffer */
#define  UC_EDIT_HFLIP              (0x0Cu)

/** Usecase Identification to save vertically flipped image of the captured buffer  */
#define  UC_EDIT_VFLIP              (0x0Du)

/** Usecase Identification to save contrast enhanced image of the captured buffer using BLT */
#define  UC_ENHANCE_BLT             (0x0Eu)

/** Usecase Identification to save contrast enhanced image of the captured buffer using percentage */
#define  UC_ENHANCE_PERCENT         (0x0Fu)

/** Usecase to execute all of the usecases */
#define UC_PERFORM_ALL          (0x10u)

/** @} */

/*============================[  Global Variables  ]=====================================*/
/** \addtogroup global_variables Global Variables	  
 *  @{
 */

/** Usage of arguments passed to application for option */
const char short_options [] = "d:ho:q:W:H:I:vcu:g:b:p:s:r:x:y:";

/** Desired use case for implementation. Default is set to UC_CAPTURE */
static int usecase = UC_CAPTURE;

/** Desired angle of rotation. Default is set to 60 degrees. */
static int angle = 60;

/** Desired factor to upscale the image using factor. */
static float upscale_factor = 1.5;

/** Desired factor to downscale the image using factor. */
static float downscale_factor = 0.5;

/** Desired width of the image to resize to from source image using both height and width. */
static int newW = 1280;

/** Desired height of the image to resize to from source image using both height and width. */
static int newH = 960;

/** Gain factor to be used in basic linear transformation. Default is set to 1.2 */
static float gain = 1.2;

/** Bias factor to be used in basic linear transformation. Default is set to 10. Can be in between 0 and 10. */
static int bias = 10;

/** Percentage value to perform contrast enhancement using percentage increment. Default is set to 25. */
static int percent = 25;

/** @} */

/*===========================[  Function declarations  ]=================================*/

/** \addtogroup internal_functions Internal Functions Internal Functions	  
 *  @{
 */

/**
 * @brief Helper function to parse and assign parameters from CLI.
 * 
 * @param[inout] argc   Input argument count
 * @param[inout] argv   Input argument vector
 * 
 */
void ParseArguments(int argc, char **argv);

/**
 * @brief   Helper function to print usage information 
 * 
 * @param[in] fp    File pointer
 * @param[in] argc  Input argument count
 * @param[in] argv  Input argument vector
 * 
 */
void usage(FILE* fp, int argc, char** argv);

/**
 * @brief   Checks if the filename has been provided for the CLI when running the PiCam library. 
 * 
 * @param[in] fname 
 * @param[in] argc  Input argument count
 * @param[in] argv  Input argument vector 
 * 
 */
void CheckValidationFilename (char* fname, int argc, char** argv);

/**
 * @brief   Function to parse input option for use#define  implementation. 
 * 
 * @param[in] algo  Usecase Id 
 * 
 */
void ExecuteUsecase(int algo);

/**
 * @brief 
 * 
 */
void SaveImage(void);

/**
 * @brief 
 * 
 */
void SaveSobel(void);

/**
 * @brief 
 * 
 */
void SaveCanny(void);

/**
 * @brief 
 * 
 */
void SaveGaussian(void);

/**
 * @brief 
 * 
 */
void SaveMean3(void);

/**
 * @brief 
 * 
 */
void Save_Mean5(void);

/**
 * @brief 
 * 
 */
void SaveMedian3(void);

/**
 * @brief 
 * 
 */
void SaveMedian5(void);

/**
 * @brief 
 * 
 */
void SaveRotate(void);

/**
 * @brief 
 * 
 */
void SaveUpscale(void);

/**
 * @brief 
 * 
 */
void SaveDownscale(void);

/**
 * @brief 
 * 
 */
void SaveResize(void);

/**
 * @brief 
 * 
 */
void SaveHFlip(void);

/**
 * @brief 
 * 
 */
void SaveVFlip(void);

/**
 * @brief 
 * 
 */
void SaveEnhancedBLT(void);

/**
 * @brief 
 * 
 */
void SaveEnhancedPercent(void);

/** @} */

#endif

/*==============================[  End of File  ]========================================*/
