/**
 * @file PiCam_App.c
 * @author Prakash Dhungana (dhunganaprakas@gmail.com)
 * @brief <b> Implementation of main application </b>
 * @version 0.1
 * @date 2022-04-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/** Doxygen compliant formatting for comments */

/*===========================[  Inclusions  ]=============================================*/

#include <getopt.h>
#include <signal.h>
#include <math.h>
#include "PiCam.h"
#include "PiCam_App.h"
#include "ColorConversion.h"
#include "write.h"
#include "Convolutions.h"
#include "Edit.h"

/*============================[  Global Constants  ]====================================*/

/** \addtogroup global_constants	Global Constants 
 *  @{
 */

/** Usage of arguments passed to application */
static const struct option long_options [] = 
{
	{ "device",     required_argument,      NULL,           'd' },
	{ "help",       no_argument,            NULL,           'h' },
	{ "output",     required_argument,      NULL,           'o' },
	{ "quality",    required_argument,      NULL,           'q' },
	{ "width",      required_argument,      NULL,           'W' },
	{ "height",     required_argument,      NULL,           'H' },
	{ "interval",   required_argument,      NULL,           'I' },
	{ "version",	no_argument,			NULL,			'v' },
	{ "continuous",	no_argument,			NULL,			'c' },
	{ "usecase",	required_argument,		NULL,			'u' },
	{ "gain",	    required_argument,		NULL,			'g' },
	{ "bias",	    required_argument,		NULL,			'b' },
	{ "percent",	required_argument,		NULL,			'p' },
	{ 0, 0, 0, 0 }
};

/** @}*/

/*===========================[  Function definitions  ]=================================*/

/** \addtogroup internal_functions Internal Functions	  
 *  @{
 */

void usage(FILE* fp, int argc, char** argv)
{
	fprintf(fp,
		"Usage: %s [options]\n\n"
		"Options:\n"
		"-d | --device name   Video device name [/dev/video0]\n"
		"-h | --help          Print this message\n"
		"-o | --output        Set JPEG output filename\n"
		"-q | --quality       Set JPEG quality (0-100)\n"
		"-W | --width         Set image width\n"
		"-H | --height        Set image height\n"
		"-I | --interval      Set frame interval (fps) (-1 to skip)\n"
		"-c | --continuous    Do continuos capture, stop with SIGINT.\n"
		"-v | --version       Print version\n"
		"-u | --usecase       Usecase ID to execute\n"
		"-g | --gain          Gain parameter value for Usecase ID UC_ENHANCE_BLT\n"
		"-b | --bias          Bias parameter value for Usecase ID UC_ENHANCE_BLT\n"
		"-p | --percent       Percentage parameter value for Usecase ID UC_ENHANCE_BLT\n"
		"-s | --scale         Scaling factor\n"
		"-r | --rotate        Rotate Image\n"
		"",
		argv[0]);
}


void ParseArguments(int argc, char **argv)
{
	for (;;) 
	{
		int index, c = 0;
		c = getopt_long(argc, argv, short_options, long_options, &index);
		if (-1 == c)
			break;

		switch (c) 
		{
			case 0: /* getopt_long() flag */
				break;

			case 'd':
				/* In case of multiple camera, sets capture device */
				deviceName = optarg;
				break;

			case 'h':
				/* Prints help on usage from CLI */
				usage(stdout, argc, argv);
				exit(EXIT_SUCCESS);

			case 'o':
				/* Set saved image filename */
				filename = optarg;
				break;

			case 'q':
				/* Sets saved image JPEG quality */
				jpegQuality = atoi(optarg);
				break;

			case 'W':
				/* Sets captured image width */
				width = atoi(optarg);
				break;

			case 'H':
				/* Sets captured image height */
				height = atoi(optarg);
				break;
				
			case 'I':
				/* Sets fps */
				fps = atoi(optarg);
				break;

			case 'c':
				/* Sets flag for continuous capture */
				continuous = 1;
				InstallSIGINTHandler();
				break;
				
			case 'v':
				/* Prints version information */
				printf("Version V0.1.0 \nDate: 2022-04-12\n");
				exit(EXIT_SUCCESS);
				break;

			case 'u':
				usecase = atoi(optarg);
				break;

			case 'p':
				percent = atoi(optarg);
				printf("Percentage: %d\n",percent);
				break;

			case 'g':
				gain = atof(optarg);
				break;

			case 'b':
				bias = atoi(optarg);
				break;	

			case 's':
				if (1 > (atof(optarg)))
					downscale_factor = atof(optarg);
				else
					upscale_factor = atof(optarg);
				break;

			case'r':
				angle = atof(optarg);
				break;

			case'x':
				newW = atoi(optarg);
				break;

			case'y':
				newH = atoi(optarg);
				break;

			default:
				/* Prints usage formats from CLI */
				usage(stderr, argc, argv);
				exit(EXIT_FAILURE);
		}			
	}
}


void CheckValidationFilename (char* fname, int argc, char** argv)
{
	/** Checks for required parameters and prints help if not in order */
	if (!fname) 
	{
		fprintf(stderr, "You have to specify JPEG output filename!\n\n");
		usage(stdout, argc, argv);
		exit(EXIT_FAILURE);
	}
}

void SaveImage(void)
{
	struct buffer Image_Save;
	Image_Save.start = malloc(width*height*3);
	Convert_YUV420toYUV444(width, height, Image_Buffer.start, Image_Save.start);
	writejpegimageYUV(width, height, Image_Save.start, filename);
}


void SaveSobel(void)
{
	struct buffer Image_grayscale;
	char* img_name = "sobel";
	Image_grayscale.start = malloc(width*height);
	memcpy(Image_grayscale.start, Image_Buffer.start, width*height);	
	Edge_Detector(width, height,Image_Buffer.start, Image_grayscale.start, METHOD_SOBEL);
	writejpeggrayscale(width, height, Image_grayscale.start, img_name);
}


void SaveCanny(void)
{
	struct buffer Image_gaussian;
	struct buffer Image_canny;
	char* img_name = "canny";
	Image_gaussian.start = malloc(width*height);
	Image_canny.start = malloc(width*height);
	GaussianFilter(width, height, Image_Buffer.start, Image_gaussian.start);	
	Edge_Detector(width, height,Image_gaussian.start, Image_canny.start, METHOD_CANNY);
	writejpeggrayscale(width, height, Image_canny.start, img_name);
}


void SaveGaussian(void)
{
	struct buffer Image_gaussian;
	char* img_name = "gaussian";
	Image_gaussian.start = malloc(width*height);
	GaussianFilter(width, height, Image_Buffer.start, Image_gaussian.start);	
	writejpeggrayscale(width, height, Image_gaussian.start, img_name);
}


void SaveMean3(void)
{
	struct buffer Image_mean;
	char* img_name = "mean3";
	Image_mean.start = malloc(width*height);
	MeanFilter(width, height, Image_Buffer.start, Image_mean.start, 3);	
	writejpeggrayscale(width, height, Image_mean.start, img_name);
}


void Save_Mean5(void)
{
	struct buffer Image_mean;
	char* img_name = "mean5";
	Image_mean.start = malloc(width*height);
	MeanFilter(width, height, Image_Buffer.start, Image_mean.start, 5);	
	writejpeggrayscale(width, height, Image_mean.start, img_name);
}


void SaveMedian3(void)
{
	struct buffer Image_median;
	char* img_name = "median3";
	Image_median.start = malloc(width*height);
	MedianFilter(width, height, Image_Buffer.start, Image_median.start, 3);	
	writejpeggrayscale(width, height, Image_median.start, img_name);
}


void SaveMedian5(void)
{
	struct buffer Image_median;
	char* img_name = "median5";
	Image_median.start = malloc(width*height);
	MedianFilter(width, height, Image_Buffer.start, Image_median.start, 5);	
	writejpeggrayscale(width, height, Image_median.start, img_name);
}


void SaveRotate(void)
{
	struct buffer Image_rotate, Image_444;
	char* img_name = "rotate";
	Image_rotate.start = malloc(width*height*3);
	Image_444.start = malloc(width*height*3);
	Convert_YUV420toYUV444(width, height, Image_Buffer.start, Image_444.start);
	Rotate_Image(width, height, Image_444.start, Image_rotate.start, angle);	
	writejpegimageYUV(width, height, Image_rotate.start, img_name);
}


void SaveUpscale(void)
{
	struct buffer Image_444;
	char* img_name = "upscale";
	Image_444.start = malloc(width*height*3);
	Convert_YUV420toYUV444(width, height, Image_Buffer.start, Image_444.start);
	Resized_Image Image_upscale = ScaleImage(width, height, Image_444.start, upscale_factor);	
	writejpegimageYUV(Image_upscale.width, Image_upscale.height, Image_upscale.start, img_name);
}


void SaveDownscale(void)
{
	struct buffer Image_444;
	char* img_name = "downscale";
	Image_444.start = malloc(width*height*3);
	Convert_YUV420toYUV444(width, height, Image_Buffer.start, Image_444.start);
	Resized_Image Image_downscale = ScaleImage(width, height, Image_444.start, downscale_factor);	
	writejpegimageYUV(Image_downscale.width, Image_downscale.height, Image_downscale.start, img_name);
}


void SaveResize(void)
{
	struct buffer Image_444;
	char* img_name = "resize";
	Image_444.start = malloc(width*height*3);
	Convert_YUV420toYUV444(width, height, Image_Buffer.start, Image_444.start);
	Resized_Image Image_resize = ResizeImage(width, height, Image_444.start, newW, newH);	
	writejpegimageYUV(Image_resize.width, Image_resize.height, Image_resize.start, img_name);
}


void SaveHFlip(void)
{
	struct buffer Image_flip, Image_444;
	char* img_name = "flip_horizontal";
	Image_flip.start = malloc(width*height*3);
	Image_444.start = malloc(width*height*3);
	Convert_YUV420toYUV444(width, height, Image_Buffer.start, Image_444.start);
	HorizontalFlip(width, height, Image_444.start, Image_flip.start);	
	writejpegimageYUV(width, height, Image_flip.start, img_name);
}


void SaveVFlip(void)
{
	struct buffer Image_flip, Image_444;
	char* img_name = "flip_vertical";
	Image_flip.start = malloc(width*height*3);
	Image_444.start = malloc(width*height*3);
	Convert_YUV420toYUV444(width, height, Image_Buffer.start, Image_444.start);
	VerticalFlip(width, height, Image_444.start, Image_flip.start);	
	writejpegimageYUV(width, height, Image_flip.start, img_name);
}

void SaveEnhancedBLT(void)
{
	struct buffer Image_blt, Image_YUV, Image_RGB;
	char* img_name = "enhanced_BLT";
	Image_blt.start = malloc(width*height*3);
	Image_YUV.start = malloc(width*height*3);
	Image_RGB.start = malloc(width*height*3);
	Convert_YUV420toYUV444(width, height, Image_Buffer.start, Image_YUV.start);
	Convert_YUV444toRGB444(width, height, Image_YUV.start, Image_RGB.start);
	ContrastEnhancement_BLT(width, height, Image_RGB.start, Image_blt.start, gain, bias);	
	writejpegimageRGB(width, height, Image_blt.start, img_name);
}


void SaveEnhancedPercent(void)
{
	struct buffer Image_percent, Image_YUV, Image_RGB;
	char* img_name = "enhanced_percent";
	Image_percent.start = malloc(width*height*3);
	Image_YUV.start = malloc(width*height*3);
	Image_RGB.start = malloc(width*height*3);
	Convert_YUV420toYUV444(width, height, Image_Buffer.start, Image_YUV.start);
	Convert_YUV444toRGB444(width, height, Image_YUV.start, Image_RGB.start);
	ContrastEnhancement_Percent(width, height, Image_RGB.start, Image_percent.start, percent);	
	writejpegimageRGB(width, height, Image_percent.start, img_name);
}


void ExecuteUsecase(int algo)
{
	switch(algo)
	{
		case UC_EDGE_SOBEL:
			SaveSobel();
			break;

		case UC_EDGE_CANNY:
			SaveCanny();
			break;

		case UC_FILTER_GAUSSIAN:
			SaveGaussian();
			break;

		case UC_FILTER_MEAN3:
			SaveMean3();
			break;
		
		case UC_FILTER_MEAN5:
			Save_Mean5();
			break;

		case UC_FILTER_MEDIAN3:
			SaveMedian3();
			break;

		case UC_FILTER_MEDIAN5:
			SaveMedian5();
			break;

		case UC_EDIT_ROTATE:
			SaveRotate();
			break;

		case UC_EDIT_UPSCALE:
			SaveUpscale();
			break;

		case UC_EDIT_DOWNSCALE:
			SaveDownscale();
			break;

		case UC_EDIT_RESIZE:
			SaveResize();
			break;

		case UC_EDIT_HFLIP:
			SaveHFlip();
			break;

		case UC_EDIT_VFLIP:
			SaveVFlip();
			break;

		case UC_ENHANCE_BLT:
			SaveEnhancedBLT();
			break;

		case UC_ENHANCE_PERCENT:
			SaveEnhancedPercent();
			break;

		default:
			printf("Invalid ID provided. \n");
			exit(EXIT_FAILURE);
	}

}

/** @} */

/*=======================[  Main Application  ]===============================*/

/** \addtogroup mainapp	Main Application 
 *  @{
 */

/**
 * @brief Main app for PiCam library.
 * 
 * @param[inout] argc	Input argument count
 * @param[inout] argv 	Input argument vector
 * 
 * @return int Return Status 
 * @retval EXIT_SUCCESS	Returned successfully
 * @retval EXIT_FAILURE	Error encountered
 *  
 */
   
int main(int argc, char **argv)
{
	int temp = 0;

	ParseArguments(argc, argv);
	CheckValidationFilename (filename, argc, argv);
	CheckContinuousFlag(continuous);
	pixel_format = V4L2_PIX_FMT_YUV420;
	
	OpenCamera();
	InitCamera(width, height);
	StartCapture();
	CaptureFrame();
	StopCapture();
	DeInitCamera();
	CloseCamera();
	
	SaveImage();
	if(usecase == UC_CAPTURE)
	{
		printf("Default use case for saving captured image\n");
	}
	else if(usecase == UC_PERFORM_ALL)
	{
		for(temp = 1; temp < UC_PERFORM_ALL; temp++)
		{
			ExecuteUsecase(temp);
		}
	}
	else
	{
		ExecuteUsecase(usecase);
	}

	exit(EXIT_SUCCESS);
	return EXIT_SUCCESS;
}

/** @} */

/*==============================[  End of File  ]======================================*/
