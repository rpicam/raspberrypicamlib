\page Usecases

# Usecases

It is assumed that the setup and build processes for this library is already completed.

Before running this command, please connect the camera device and enable the camera interface from Raspberry Pi preferences. If it is Ubuntu, 
check if the camera device is available from list of connected devices. This step is the actual step which captures the image and saves the 
output to  <Repository_root>/Build/<filename>.jpg. Open the folder and check if the image file is saved. Finally check if capture was successful 
or not by opening the image.

To find information on what image size are supported run:

```
    v4l2-ctl -d /dev/video0 --list-formats-ext
```

The default height and width of the captured image is set to (640x480). 

##  Capture and Save (UC_CAPTURE)

CLI command:

```
    ./PiCam_App -o capture
```

Optional Using UC option:
```
    ./PiCam_App -o capture -u 0
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is optional for this usecase. This command saves the image named "capture.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required. 
```
    ./PiCam_App -o capture -W 1280 -H 720
```

##  Save Sobel Edges (UC_EDGE_SOBEL)

CLI command:
```
    ./PiCam_App -o capture -u 1
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "sobel.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -u 1 -W 1280 -H 720
```

##  Save Canny Edges (UC_EDGE_CANNY)

CLI command:
```
    ./PiCam_App -o capture -u 2
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "canny.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 2
```

##  Save Gaussian filter (UC_FILTER_GAUSSIAN)

CLI command:
```
    ./PiCam_App -o capture -u 3
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 3
```

##  Save Mean Filtered Image with Kernel Size 3 (UC_FILTER_MEAN3)

CLI command:
```
    ./PiCam_App -o capture -u 4
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 4
```

##  Save Mean Filtered Image with Kernel Size 5 (UC_FILTER_MEAN5)

CLI command:
```
    ./PiCam_App -o capture -u 5
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 5
```

##  Save Median Filtered Image with Kernel Size 3 (UC_FILTER_MEDIAN3)

CLI command:
```
    ./PiCam_App -o capture -u 6
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 6
```

##  Save Median Filtered Image with Kernel Size 5 (UC_FILTER_MEDIAN5)

CLI command:
```
    ./PiCam_App -o capture -u 7
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 7
```

##  Save Rotated Image (UC_EDIT_ROTATE)

CLI command:
```
    ./PiCam_App -o capture -u 8
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 8
```

The default angle of rotation of the image is set to 60 degrees. The valid range for rotation angle is 0 to 360 inclusive. To enable rotating image 
using CLI option(e.g. 245), following command is used:
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 8 -r 245
```

##  Save Up-Scaled Image (UC_EDIT_UPSCALE)

CLI command:
```
    ./PiCam_App -o capture -u 9
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 9
```

The default upscale factor is set to 1.5. The valid range for scaling factor is 1 to 5 inclusive. If the value is less than 1, an image named 
"upscale" is saved with default upscaled factor. To enable scaling using CLI option(e.g. 2.2), following command is used:
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 10 -s 2.2
```

##  Save Down-Scaled Image (UC_EDIT_DOWNSCALE)

CLI command:
```
    ./PiCam_App -o capture -u 10
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 10
```

The default upscale factor is set to 0.5. The valid range for scaling factor is 0 to 1 inclusive. If the value is less than 1, an image named 
"downscale" is saved with default down scaled factor. To enable scaling using CLI option(e.g. 0.8), following command is used:
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 10 -s 0.8
```

##  Save Resized Image (UC_EDIT_RESIZE)

CLI command:
```
    ./PiCam_App -o capture -u 11
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 11
```

The default width and height of the resized image is set to 1280and 960 respectively. To enable resizing an image using CLI option(e.g. 1680x1080), 
following command is used:
```
    ./PiCam_App -o capture -u 11 -x 1680 -y 1080
```

If only one of the option is used, the image is stretched/compressed in one dimension only. This usecase can be used for both upscaling and 
downscaling operations. The option 'x' is used to modify width of the image and 'y' is used to modify the height of the image.

##  Flip Image Horizontally (UC_EDIT_HFLIP)

CLI command:
```
    ./PiCam_App -o capture -u 12
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 12
```

##  Flip Image Vertically (UC_EDIT_VFLIP)

CLI command:
```
    ./PiCam_App -o capture -u 13
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 13
```

##  Enhance Contrast using Basic Linear Transformation (UC_ENHANCE_BLT)

CLI command:
```
    ./PiCam_App -o capture -u 14
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 14
```

The default gain and bias is set to 1.2 and 10 respectively. The valid range for gain is 1 to 3 and similarly for bias is 0 to 10. To enable contrast
 enhancement using percentage increment(e.g. bias of 1.5 and bias of 8), following command is used:
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 14 -g 1.5 -b 8
```

##  Enhance Contrast using Percent Increment (UC_ENHANCE_PERCENT)

CLI command:
```
    ./PiCam_App -o capture -u 15
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 15
```

The default percentage increment is set to 25%. The valid range for enhancement is 1 to 100 inclusive. To enable contrast enhancement using 
percentage increment(e.g. 50%), following command is used:
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 15 -p 50
```

##  Perform all available Use-Cases (UC_PERFORM_ALL)

CLI command:
```
    ./PiCam_App -o capture -u 16
```

Since, the default usecase is set for UC_CAPTURE, the usecase option is mandatory for this usecase. This command saves the image named "gaussian.jpg" 
in  <Repository_root>/Build/ . To capture the image in desired dimensions, following command options is required.
```
    ./PiCam_App -o capture -W 1280 -H 720 -u 16
```

